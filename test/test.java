package com.expanion.service.dal;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

public class DynamoDBService {

	private static final DynamoDBMapper MAPPER;

	static {
		AmazonDynamoDBClient client = new AmazonDynamoDBClient();
		client.setRegion(Regions.AP_SOUTHEAST_1);
		MAPPER = new DynamoDBMapper(client);
	}

	public static DynamoDBMapper getMapper() {
		return MAPPER;
	}

}
