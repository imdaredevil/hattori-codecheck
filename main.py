from flask import *
from functools import wraps
import mammoth
import csv
import os
import shutil
import random
import subprocess

app = Flask("sync-olpc")
app.secret_key = 'secretkey'
level = 1;

def cchecker(code,qid,rollno):
	fil = open("./ansimdaredevil/stest/" + str(qid) + "/testcases","r");
	wfil = open("./test/testcases","w");
	f = fil.read();
	print(f)
	wfil.write(f);
	fil.close();
	wfil.close();
	subprocess.call(["bash","./test/Ccheck","./test/test.cpp","./test/testcases","./test/test"]);
	fil = open("./ansimdaredevil/sout/" + str(qid) + "/out.txt","r");
	out = fil.read()
	print(fil)
	tes = open("./test/out.txt","r");
	tes = tes.read();
	tes = tes.strip("\n");
	out = out.strip("\n");
	tes = tes.strip(" ");
	out = out.strip(" ");
	if out == tes:
		code = open("./test/test.cpp","r");
		code1 = open("./hack/" + str(qid) + "/" + rollno,"w");
		lang = open("./hack/" + str(qid) + "/lang" + rollno,"w");
		lang.write("C");
		lang.close();
		code1.write(code.read());
		code.close();
		code1.close();
	subprocess.call(["rm", "./test/test.cpp","./test/out.txt"]);
	if out == tes:
		return 1
	else:
		return 0

def jchecker(code,qid,rollno):
	fil = open("./ansimdaredevil/stest/" + str(qid) + "/testcases","r");
	wfil = open("./test/testcases","w");
	wfil.write(fil.read());
	fil.close();
	wfil.close();
	subprocess.call(["bash","./test/Jcheck","./test/test.java","./test/testcases"]);
	fil = open("./ansimdaredevil/sout/" + str(qid) + "/out.txt","r");
	out = fil.read()
	tes = open("./test/out.txt");
	try:
		tes = tes.read();
		tes = tes.strip("\n");
		out = out.strip("\n");
		tes = tes.strip(" ");
		out = out.strip(" ");
		if out == tes:
			code = open("./test/test.java");
			code1 = open("./hack/" + str(qid) + "/" + rollno,"w");
			lang = open("./hack/" + str(qid) + "/lang" + rollno,"w");
			lang.write("java");
			lang.close();
			code1.write(code.read());
			code.close();
			code1.close();
			subprocess.call(["rm","./test/test.java","./test/out.txt"]);
		if out == tes:
			return 1
		else:
			return 0
	except:
		return -1;

def pychecker(code,qid,rollno):
	fil = open("./ansimdaredevil/stest/" + str(qid) + "/testcases","r");
	wfil = open("./test/testcases","w");
	wfil.write(fil.read());
	fil.close();
	wfil.close();
	subprocess.call(["bash","./test/Pycheck","./test/test.py","./test/testcases"]);
	fil = open("./ansimdaredevil/sout/" + str(qid) + "/out.txt","r");
	out = fil.read()
	tes = open("./test/out.txt");
	try:
		tes = tes.read();
		print(tes)
		tes = tes.strip("\n");
		out = out.strip("\n");
		tes = tes.strip(" ");
		out = out.strip(" ");
		if out == tes:
			code = open("./test/test.py");
			code1 = open("./hack/" + str(qid) + "/" + rollno,"w");
			lang = open("./hack/" + str(qid) + "/lang" + rollno,"w");
			lang.write("python");
			lang.close();
			code1.write(code.read());
			code.close();
			code1.close();
		subprocess.call(["rm","./test/test.py","./test/out.txt"]);
		if out == tes:
			return 1
		else:
			return 0
	except:
		return -1;


def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'rollno' in session and session["rollno"] and "level" not in os.listdir("."):
            return f(*args, **kwargs)
        else:
            return redirect(url_for("index"))
    return wrap

def is_logged_2in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        files = os.listdir("./")
        if "level" in files:
            return f(*args, **kwargs)
        else:
            return redirect(url_for("index"))
    return wrap

@app.route("/questions")
@is_logged_in
def questions():
	message = request.args.get("message");
	if message == None:
		message = None
	rollno = session["rollno"];
	status = []
	qus = os.listdir("./hack")
	for q in sorted(qus):
		lis = os.listdir("./hack/" + q  + "/")
		if rollno in lis:
			status.append(1);
		else:
			status.append(0);
	fil = open("./questions/questions.txt");
	f = fil.read()
	f = f.split(";")
	quesions = []
	for i in range(0,len(qus)):
		quesions.append({"qid": i + 1, "name" : f[i] , "status" : status[i] })
	return render_template("questions.html",questions = quesions,message = message)

@app.route("/",methods=["GET","POST"])
def index():
	if 'rollno' in session and session["rollno"]:
		return redirect(url_for('questions'));
	if(request.method == "GET"):
		return render_template("index.html");
	rollno = request.form["rollno"];
	if "level" not in os.listdir("."):
		session["rollno"] = rollno;
	return redirect(url_for('questions'));


@app.route("/question")
@is_logged_in
def display():
	qid = request.args.get("qid");
	fil = open("./questions/" + qid + ".txt");
	f = fil.read();
	lis = os.listdir("./hack/" + str(qid) + "/");
	status = 0
	if session["rollno"] in  lis:
		status = 1 
	return render_template("question.html",fil = f,qid = qid,status = status);



@app.route("/validate",methods=["POST","GET"])
@is_logged_in
def validate():
	if "code" not in request.files:
		return redirect(url_for("questions",message="Code not uploaded or language not chosen"));
	code = request.files["code"];
	qid = request.form["qid"];
	lang = request.form["lang"];
	val = 0;
	if(lang == "python"):
		code.save("./test/test.py");
		val = pychecker(code,qid,session["rollno"]);
	if(lang == "java"):
		code.save("./test/test.java");
		val = jchecker(code,qid,session["rollno"]);
	if(lang == "cpp"):
		code.save("./test/test.cpp");
		val = cchecker(code,qid,session["rollno"]);
	message = "Correct"
	if (val == -1):
		message = "error in compilation"
	elif (val == 1):
		message = "correct answer"
	else:
		message = "wrong answer"
	return redirect(url_for("questions",message=message));


#----------------------------------------------HACKER------------------------------------------------------

@app.route("/imdaredevil/")
@is_logged_2in
def routes():
	files = os.listdir("./hack/")
	filesll = []
	for f in files:
		filesll.append({"name":f , "link": "/imdaredevil/" + f})
	return render_template("listfiles.html",files=filesll)

@app.route("/imdaredevil/<kname>")
@is_logged_2in
def route(kname):
	pa = "./hack/" + kname;
	if not os.path.exists(pa):
		return redirect(url_for("routes"))
	if os.path.isdir(pa):
		files = os.listdir(pa)
		filesll = []
		for f in files:
			filesll.append({"name": f , "link": "/imdaredevil/" + kname + "/" + f})
		return render_template("listfiles.html",files=filesll)


@app.route("/imdaredevil/<qid>/<kname>")
@is_logged_2in
def route2(qid,kname):
	pa = "./hack/" + qid + "/" + kname;
	file = open(pa,"r");
	file = file.read();
	file = file.split("\n")
	return render_template("file.html",file=file,qid=qid,filename=kname)



def ccheck(qid,rollno):
	fil = open("./hack/" + str(qid) + "/" + rollno)
	wfil = open("./test/test.cpp","w")
	f = fil.read()
	wfil.write(f)
	fil.close()
	wfil.close()
	fil = open("./test/test.cpp","r");
	print(fil.read())
	fil.close()
	fil = open("./ansimdaredevil/test/" + str(qid) + "/testcases","r");
	wfil = open("./test/testcases","w");
	f = fil.read();
	wfil.write(f);
	fil.close();
	wfil.close();
	subprocess.call(["bash","./test/Ccheck","./test/test.cpp","./test/testcases","./test/test"]);
	fil = open("./ansimdaredevil/out/" + str(qid) + "/out.txt","r");
	out = fil.read()
	tes = open("./test/out.txt","r");
	tes = tes.read();
	print(tes)
	tes = tes.strip("\n");
	out = out.strip("\n");
	tes = tes.strip(" ");
	out = out.strip(" ");
	if out == tes:
		code = open("./test/test.cpp","r");
		code1 = open("./hack/" + str(qid) + "/" + rollno,"w");
		lang = open("./hack/" + str(qid) + "/lang" + rollno,"w");
		lang.write("C");
		lang.close();
		code1.write(code.read());
		code.close();
		code1.close();
	subprocess.call(["rm", "./test/test.cpp","./test/out.txt"]);
	if out == tes:
		return 1
	else:
		return 0

def jcheck(qid,rollno):
	fil = open("./hack/" + str(qid) + "/" + rollno)
	wfil = open("./test/test.java","w")
	wfil.write(fil.read())
	fil.close()
	wfil.close()
	fil = open("./ansimdaredevil/test/" + str(qid) + "/testcases","r");
	wfil = open("./test/testcases","w");
	wfil.write(fil.read());
	fil.close();
	wfil.close();
	subprocess.call(["bash","./test/Jcheck","./test/test.java","./test/testcases"]);
	fil = open("./ansimdaredevil/out/" + str(qid) + "/out.txt","r");
	out = fil.read()
	tes = open("./test/out.txt");
	try:
		tes = tes.read();
		tes = tes.strip("\n");
		out = out.strip("\n");
		tes = tes.strip(" ");
		out = out.strip(" ");
		if out == tes:
			code = open("./test/test.java");
			code1 = open("./hack/" + str(qid) + "/" + rollno,"w");
			lang = open("./hack/" + str(qid) + "/lang" + rollno,"w");
			lang.write("java");
			lang.close();
			code1.write(code.read());
			code.close();
			code1.close();
			subprocess.call(["rm","./test/test.java","./test/out.txt"]);
		if out == tes:
			return 1
		else:
			return 0
	except:
		return -1;

def pycheck(qid,rollno):
	fil = open("./hack/" + str(qid) + "/" + rollno)
	wfil = open("./test/test.py","w")
	wfil.write(fil.read())
	fil.close()
	wfil.close()
	fil = open("./ansimdaredevil/test/" + str(qid) + "/testcases","r");
	wfil = open("./test/testcases","w");
	wfil.write(fil.read());
	fil.close();
	wfil.close();
	subprocess.call(["bash","./test/Pycheck","./test/test.py","./test/testcases"]);
	fil = open("./ansimdaredevil/out/" + str(qid) + "/out.txt","r");
	out = fil.read()
	tes = open("./test/out.txt");
	try:
		tes = tes.read();
		tes = tes.strip("\n");
		out = out.strip("\n");
		tes = tes.strip(" ");
		out = out.strip(" ");
		if out == tes:
			code = open("./test/test.py");
			code1 = open("./hack/" + str(qid) + "/" + rollno,"w");
			lang = open("./hack/" + str(qid) + "/lang" + rollno,"w");
			lang.write("python");
			lang.close();
			code1.write(code.read());
			code.close();
			code1.close();
		subprocess.call(["rm","./test/test.py","./test/out.txt"]);
		if out == tes:
			return 1
		else:
			return 0
	except:
		return -1;



@app.route("/resultomania")
@is_logged_in
def result():
	sc = {}
	for i in os.listdir("./hack/"):
		rollno = session["rollno"]
	
		score = open("./hack/" + i + "/score" + rollno,"w")
		if rollno in os.listdir("./hack/" + i + "/"):
			lang = open("./hack/" + i + "/lang" + rollno)
			lang = lang.read()
			value = 0
			if (lang == "C"):
				value = ccheck(i,rollno)
			elif (lang == "python"):
				value = pycheck(i,rollno)
			elif (lang == "java"):
				value = jcheck(i,rollno)
			if value == 1:
				sc[i] = 100
				score.write(str(100))
			else:
				sc[i] = 0
				score.write(str(0))
		else:
			sc[i] = 0
			score.write(str(0))
		score.close()
	print(sc)
	return render_template("result.html",scores=sc)


if __name__ == '__main__':
		app.secret_key = 'secretkey'
		app.run(host='0.0.0.0',debug=True,port=8080);

