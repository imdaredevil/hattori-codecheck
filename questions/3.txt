Description

Sukin is a math wizard. He loves to play with math. Sukin's friend palani is a marana coder. One day when palani was solving problems in
codechef, he  was solving a problem using X-OR. Seeing this, Sukin decided to test palani's skills and gave him a puzzle. Given an array
of numbers, a pair is said to be vera level if the X-OR value of the numbers can be expressed as a sum of two prime numbers of same parity.
(two numbers have the same parity if they produce the same reminder when divided by 2). Given an array of numbers, help palani to find the
number of such vera level pairs in that array.

Input format
first line contains T denoting the number of test cases.The next 2T lines are as follows:-
       first line contains a single integer n denoting the number of elements in the array.
       second line contains n spaced integers corresponding to the elements of the array.

Output format

T lines with each line i containing a single integer denoting the number of vera level pairs corresponding to test case Ti.

Constraints

1<=n,a[i]<=100000

Sample input 0

2
6
1 2 3 4 5 6
3
2 8 16

Sample output 0

4
3

Explanation
Test case 1: vera level pairs are (1,5) , (2,4) , (3,5) , (2,6)
Test case 2: vera level pairs are (2,8) , (2,16) , (8,16)
 